import React, { useState } from 'react';
import { Form, Button } from 'react-bootstrap';


	const [courseId, setcourseId] = useState('');
    const [courseName, setCourseName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState(0);
    const [startDate, setStartDate] = useState('');
    const [endDate, setEndDate] = useState('');

 function createCourse(e){
 	e.preventDefault();


 	        console.log(`${courseName} with ID: ${courseId} is set to start on ${startDate} for PhP ${price} per slot.`);

 	 setcourseId('');
     setCourseName('');
     setDescription('');
     setPrice(0);
     setStartDate('');
     setEndDate('');

 }

return(
<Form onSubmit={(e) => createCourse(e)}>
	<Form.Group controlId="courseId">
	    <Form.Label>course ID</Form.Label>
	    <Form.Control type="text" placeholder="PLease insert course ID" value={courseId} onChange={e => setcourseId(e.target.value)}  required/>
	    <Form.Text className="text-muted">
	    
	    </Form.Text>
	</Form.Group>

	    <Form.Group controlId="courseName">
	        <Form.Label>Course Name</Form.Label>
	        <Form.Control type="text" placeholder="Please Insert Name" value={courseName} onChange={e => setCourseName(e.target.value)} required/>
	    </Form.Group>

	      <Form.Group controlId="description">
	        <Form.Label>Description</Form.Label>
	        <Form.Control type="text" placeholder="Please Insert Description" value={description} onChange={e => setDescription(e.target.value)} required/>
	    </Form.Group>


	    <Form.Group controlId="price">
	        <Form.Label>Price:</Form.Label>
	        <Form.Control type="text" placeholder="Please Insert Price" value={price} onChange={e => setPrice(e.target.value)} required/>
	    </Form.Group>

	    <Form.Group controlId="startDate">
	        <Form.Label>Start Date:</Form.Label>
	        <Form.Control type="text" placeholder="Please Insert Start Date" value={startDate} onChange={e => setStartDate(e.target.value)} required/>
	    </Form.Group>

	    <Form.Group controlId="endDate">
	        <Form.Label>End Date:</Form.Label>
	        <Form.Control type="text" placeholder="Please Insert End Date" value={endDate} onChange={e => setEndDate(e.target.value)} required/>
	    </Form.Group>

	   
	    <Button variant="primary" type="submit" id="submitBtn">Submit</Button>
                    
</Form>

)
